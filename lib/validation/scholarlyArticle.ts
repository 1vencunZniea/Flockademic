import { getMinLengthValidator } from './validators';

export const scholarlyArticleDescriptionValidators = [];

export const scholarlyArticleNameValidators = [
  getMinLengthValidator<string>({ minLength: 1 }),
];

import { GetOrcidAuthorsResponse } from '../../../../lib/interfaces/endpoints/crossref';
import { getOrcidAuthors } from '../../src/services/crossref';

describe('getOrcidAuthors', () => {
  const mockResponse: GetOrcidAuthorsResponse = {
    'message': {
      'items': [ {
        DOI: 'arbitrary-doi',
        author: [ {
          'ORCID': 'arbitrary-orcid',
          'authenticated-orcid': true,
          'family': 'Arbitrary family name',
          'given': 'Arbitrary given name',
        }],
        title: [ 'Arbitrary article title' ],
      } ],
      'total-results': 1337,
    },
    'message-type': 'work-list',
    'message-version': '1.0.0',
    'status': 'ok',
  };

  it('should reject if the CrossRef API encountered an error', () => {
    const mockErrorResponse: GetOrcidAuthorsResponse = {
      status: 'error',
    };

    // fetch is mocked by jest-fetch-mock
    fetch.mockResponseOnce(JSON.stringify(mockErrorResponse));

    return expect(getOrcidAuthors()).rejects.toBeDefined();
  });

  it('should return the authors associated with the found articles that have an ORCID', () => {
    const mockMultipleArticleResponse: GetOrcidAuthorsResponse = {
      ...mockResponse,
      message: {
        ...mockResponse.message,
        items: [
          {
            DOI: 'arbitrary-first-article-doi',
            author: [
              {
                family: 'Arbitrary family name of ORCID-less author',
                given: 'Arbitrary given name of ORCID-less author',
              },
              {
                'ORCID': 'https://orcid.org/some-first-orcid',
                'authenticated-orcid': true,
                'family': 'Author',
                'given': 'First',
              },
            ],
            title: [ 'Arbitrary first article title' ],
          },
          {
            DOI: 'arbitrary-second-article-doi',
            author: [
              {
                'ORCID': 'https://orcid.org/some-second-orcid',
                'authenticated-orcid': true,
                'family': 'Author',
                'given': 'Second',
              },
              {
                'ORCID': 'https://orcid.org/arbitrary-orcid',
                'authenticated-orcid': false,
                'family': 'Arbitrary family name for unverified ORCID author',
                'given': 'Arbitrary second name for unverified ORCID author',
              },
              {
                'ORCID': 'https://orcid.org/some-third-orcid',
                'authenticated-orcid': true,
                'family': 'Author',
                'given': 'Third',
              },
            ],
            title: [ 'Arbitrary second article title' ],
          },
        ],
      },
    };

    // fetch is mocked by jest-fetch-mock
    fetch.mockResponseOnce(JSON.stringify(mockMultipleArticleResponse));

    return expect(getOrcidAuthors()).resolves.toEqual([
      { name: 'First Author', sameAs: 'https://orcid.org/some-first-orcid' },
      { name: 'Second Author', sameAs: 'https://orcid.org/some-second-orcid' },
      { name: 'Third Author', sameAs: 'https://orcid.org/some-third-orcid' },
    ]);
  });
});

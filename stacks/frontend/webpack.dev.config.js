var makeBaseConfig = require('./webpack.common.config');

var webpack = require('webpack');
var RobotstxtPlugin = require('robotstxt-webpack-plugin').default;

module.exports = function makeWebpackConfig (env) {
  var config = makeBaseConfig(env);

  config.devServer = {
    historyApiFallback: true,
    overlay: {
      errors: true,
      warnings: false,
    },
  };

  config.devtool = 'cheap-module-eval-source-map';

  config.devServer = {
    historyApiFallback: {
      rewrites: [
        // We need an explicit rule for /article/<DOI>, since
        // connect-history-api-fallback by default does not
        // redirect URLs with a dot in them (like DOIs have)
        // because it assumes them to be direct file requests.
        { from: /^\/article\/(.*?)\/(.*?)/, to: '/index.html' },
      ],
    },
    host: "0.0.0.0",
    port: 8000
  };

  config.plugins.push(
    // Reference: https://www.npmjs.com/package/robotstxt-webpack-plugin
    new RobotstxtPlugin({
      policy: [
        {
          userAgent: '*',
          disallow: '/',
        },
      ],
    })
  );

  return config;

};

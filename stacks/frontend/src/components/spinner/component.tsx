import './styles.scss';

import * as React from 'react';

export interface SpinnerState { finishedDelay: boolean; }
export interface SpinnerProps {
  size?: 'small' | 'medium' | 'large';
  invert?: boolean;
  children?: React.ReactNode;
}

export class Spinner extends React.Component<SpinnerProps, SpinnerState> {
  // Although setTimeout returns a number in the browser, it returns a NodeJS.Timer on the server:
  private timers: Array<number | NodeJS.Timer>;

  constructor(props: SpinnerProps) {
    super(props);

    this.timers = [];
  }

  public componentWillMount() {
    this.setState({ finishedDelay: false });
  }
  public componentDidMount() {
    // Users only notice slow loading after 200ms, so only show a spinner
    // if it takes longer than that.
    this.timers.push(setTimeout(() => this.setState({ finishedDelay: true }), 200));
  }
  public componentWillUnmount() {
    // Converting the timeoutId to type `any` because clearTimeout uses Node's type definitions,
    // and thus does not expect a number
    this.timers.map((timeoutId: any) => clearTimeout(timeoutId));
  }
  public render() {
    if (!this.state.finishedDelay) {
      return null;
    }

    return (
      <div className="loadingIndicator">
        <div className={`spinner ${this.props.size || 'medium'} ${this.props.invert ? 'inverted' : ''}`}>
          <span className="spinner__dot spinner__dot1"/>
          <span className="spinner__dot spinner__dot2"/>
          <span className="spinner__dot spinner__dot3"/>
          <span className="spinner__dot spinner__dot4"/>
        </div>
        {this.props.children}
      </div>
    );
  }
}

import * as React from 'react';
import { Link } from 'react-router-dom';

import { Person } from '../../../../../lib/interfaces/Person';
import { convertToOrcid } from '../../../../../lib/utils/orcid';

interface Props {
  profiles: Array<Partial<Person>>;
}

export const ProfileList = (props: Props) => (
  props.profiles.length === 0
    ? null
    : renderProfiles(props.profiles)
);

function renderProfiles(profiles: Array<Partial<Person>>) {
    const list = profiles.filter((profile) => typeof profile.sameAs === 'string').map((profile) => {
      // Tell TypeScript explicitly that we filtered it to only strings above:
      const orcid = convertToOrcid(profile.sameAs as string);

      return (
        <div
          className="media"
          itemScope={true}
          itemType="https://schema.org/Person"
          key={`article_${profile.identifier}_${orcid}`}
        >
          <p className="media-item">
            <Link
              itemProp="url"
              to={`/profile/${orcid}`}
              title={profile.description || `View the profile of ${profile.name}`}
            >
              {profile.name}
            </Link>
          </p>
        </div>
      );
    });

    return (
      <div className="profileList">
        {list}
      </div>
    );
}

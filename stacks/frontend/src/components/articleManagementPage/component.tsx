require('./styles.scss');

import * as React from 'react';
import * as ReactGA from 'react-ga';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { ThunkDispatch } from 'redux-thunk';

import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import {
  AlertState,
  completeSomething,
  CompleteSomethingAction,
  IgnoreCompleteSomethingAction,
} from '../../ducks/alert';
import { AppState } from '../../ducks/app';
import {
  getScholarlyArticle,
  publishScholarlyArticle,
  submitScholarlyArticle,
  updateScholarlyArticle,
} from '../../services/periodical';
import { FileManager } from '../fileManager/component';
import { OrcidButton } from '../orcidButton/component';
import { PageMetadata } from '../pageMetadata/component';
import { Spinner } from '../spinner/component';

interface ArticleManagementPageState {
  article?: Partial<ScholarlyArticle> | null;
  hasBeenSubmitted: boolean;
  isBeingMadePublic: boolean;
  isBeingUpdated: boolean;
  detailFormValues: {
    description: string;
    name: string;
  };
  uploadFormValues: {
    agreeWithLicense: boolean;
    file?: File;
  };
  error?: JSX.Element;
}
export interface ArticleManagementPageStateProps {
  orcid?: string;
}
export interface ArticleManagementPageDispatchProps {
  onUpdate: (successMessage: JSX.Element) => void;
}
export interface ArticleManagementPageOwnProps {}
export type ArticleManagementPageProps =
  ArticleManagementPageStateProps & ArticleManagementPageDispatchProps & ArticleManagementPageOwnProps;
export interface ArticleManagementPageRouteParams { articleId: string; }

export class BareArticleManagementPage
  extends React.Component<
  ArticleManagementPageProps & RouteComponentProps<ArticleManagementPageRouteParams>,
  ArticleManagementPageState
> {
  public state: ArticleManagementPageState = {
    article: undefined,
    detailFormValues: {
      description: '',
      name: '',
    },
    hasBeenSubmitted: false,
    isBeingMadePublic: false,
    isBeingUpdated: false,
    uploadFormValues: {
      agreeWithLicense: false,
    },
  };

  constructor(props: ArticleManagementPageProps & RouteComponentProps<ArticleManagementPageRouteParams>) {
    super(props);

    this.handleFormFieldChange = this.handleFormFieldChange.bind(this);
    this.handleFileUpload = this.handleFileUpload.bind(this);
    this.updateArticle = this.updateArticle.bind(this);
    this.makePublic = this.makePublic.bind(this);
  }

  public componentDidMount() {
    getScholarlyArticle(this.props.match.params.articleId)
    .then((article) => {
      this.setState({
        article,
        detailFormValues: {
          description: article.description || '',
          name: article.name || '',
        },
      });
    })
    .catch(() => this.setState({ article: null }));
  }

  public render() {
    return (
      <div>
        <section className="hero is-medium is-dark">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">
                Article settings
                <PageMetadata url={this.props.match.url} title="Article settings"/>
              </h1>
            </div>
          </div>
        </section>
        {this.getView()}
      </div>
    );
  }

  private getView() {
    if (typeof this.state.article === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }
    if (this.state.article === null) {
      return (
        <section className="section">
          <div className="container">
            <div className="message is-warning content">
              <div className="message-body">
                This article could not be found. Would you like to go&nbsp;
                <Link
                  to="/"
                  title="Flockademic homepage"
                >
                  back to the Flockademic homepage?
                </Link>
              </div>
            </div>
          </div>
        </section>
      );
    }

    return (
      <div className="container">
        {this.renderErrors()}
        <div className="columns">
          {this.renderArticleForm(this.state.article)}
          <div className="column is-one-third-widescreen is-offset-one-fifth-widescreen">
            {this.renderMakePublicForm()}
          </div>
        </div>
      </div>
    );
  }

  private renderArticleForm(article: Partial<ScholarlyArticle>) {
    return (
      <div className="column is-half-widescreen">
        <section className="section">
          <form onSubmit={this.updateArticle} className="details-update-form">
            <h3 className="title is-3">Details</h3>
            <div className="field">
              <label htmlFor="scholarlyArticleName" className="label">
                Article title
              </label>
              <div className="control">
                <input
                  id="scholarlyArticleName"
                  placeholder="Article title"
                  value={this.state.detailFormValues.name}
                  onChange={this.handleFormFieldChange}
                  name="name"
                  className="input"
                />
              </div>
            </div>
            <div className="field">
              <label htmlFor="scholarlyArticleDescription" className="label">
                Abstract
              </label>
              <div className="control">
                <textarea
                  id="scholarlyArticleDescription"
                  placeholder="Abstract"
                  value={this.state.detailFormValues.description}
                  onChange={this.handleFormFieldChange}
                  name="description"
                  className="textarea"
                />
              </div>
            </div>
            <div className="field is-grouped is-grouped-right">
              <div className="control">
                <Link
                  to={`/article/${this.props.match.params.articleId}`}
                  className="button is-text"
                >
                  Cancel
                </Link>
              </div>
              <div className="control">
                <button type="submit" className={`button is-link ${this.state.isBeingUpdated ? 'is-loading' : ''}`}>
                  Save
                </button>
              </div>
            </div>
          </form>
        </section>
        <section className="section">
          <h3 className="title is-3">Document</h3>
          <FileManager
            articleId={this.props.match.params.articleId}
            files={(article.associatedMedia) ? article.associatedMedia : []}
            onUpload={this.handleFileUpload}
          />
        </section>
      </div>
    );
  }

  private renderErrors() {
    if (!this.state.error) {
      return null;
    }

    return (
      <section className="section">
        <div className="message is-warning">
          <div className="message-body">
            {this.state.error}
          </div>
        </div>
      </section>
    );
  }

  private renderMakePublicForm() {
    if (!this.state.article || this.state.article.datePublished) {
      return null;
    }

    return (
      <aside className="section content">
        <form onSubmit={this.makePublic} className="notification publication-form">
          <h3 className="title">This article has not yet been published</h3>
          <p>Only you can view this article. You can submit it for publication after completing the following steps:</p>
          <ul className="publicationProgress">
            {this.renderTodoItemForDetails()}
            {this.renderTodoItemForUpload()}
            {this.renderTodoItemForAccount()}
          </ul>
          <button
            type="submit"
            className={`button is-primary ${this.state.isBeingMadePublic ? 'is-loading' : ''}`}
            disabled={!this.isReadyForPublication()}
          >
            Make public
          </button>
        </form>
      </aside>
    );
  }

  private renderTodoItemForDetails() {
    // TODO Also validate the name and abstract
    if (
      this.state.article &&
      this.state.article.name &&
      this.state.article.description
    ) {
      return (
        <li id="detailsTodo" className="publicationProgress__todo publicationProgress__todo--done">
          Provide your article title and abstract
        </li>
      );
    }

    return (
      <li id="detailsTodo" className="publicationProgress__todo">
        Provide your article title and abstract
      </li>
    );
  }

  private renderTodoItemForUpload() {
    if (
      this.state.article &&
      this.state.article.associatedMedia
    ) {
      return (
        <li id="uploadTodo" className="publicationProgress__todo publicationProgress__todo--done">
          Upload your article
        </li>
      );
    }

    return (
      <li id="uploadTodo" className="publicationProgress__todo">
        Upload your article
      </li>
    );
  }

  private renderTodoItemForAccount() {
    if (this.props.orcid) {
      return (
        <li id="accountTodo" className="publicationProgress__todo publicationProgress__todo--done">
          Create account / sign in
        </li>
      );
    }

    return (
      <li id="accountTodo" className="publicationProgress__todo">
        <OrcidButton>Create account / sign in</OrcidButton>
      </li>
    );
  }

  private handleFormFieldChange(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
    const target = event.target;
    this.setState((prevState: ArticleManagementPageState) => ({
      detailFormValues: {
        ...prevState.detailFormValues,
        [target.name]: target.value,
      },
    }));
  }

  private handleFileUpload(
    file: { name: string; contentUrl: string, license: 'https://creativecommons.org/licenses/by/4.0/' },
  ) {
    this.setState((prevState) => {
      const newState = {
        article: {
          ...prevState.article,
          associatedMedia: [ file ],
        },
      };

      return newState;
    });

    this.props.onUpdate(<span>Document uploaded successfully!</span>);

    ReactGA.event({
      action: 'Upload',
      category: 'Article Management',
    });
  }

  private async updateArticle(event: React.FormEvent<HTMLFormElement>) {
    // TODO: Only allow valid fields when the article is already public
    event.preventDefault();
    this.setState({ isBeingUpdated: true });

    ReactGA.event({
      action: 'Update details',
      category: 'Article Management',
    });

    try {
      const response = await updateScholarlyArticle(this.props.match.params.articleId, this.state.detailFormValues);
      this.setState((prevState) => ({
        article: {
          ...prevState.article,
          ...response.result,
        },
        isBeingUpdated: false,
      }));
      this.props.onUpdate((
        <span>
          Saved!
        </span>
        ));
    } catch (e) {
      this.setState({
        error: <span>There was a problem updating the article details, please try again.</span>,
        isBeingUpdated: false,
      });
    }
  }

  private isReadyForPublication(): boolean {
    // TODO Validate the article details
    return !!this.state.article &&
      !!this.state.article.name &&
      !!this.state.article.description &&
      !!this.state.article.associatedMedia &&
      !!this.props.orcid;
  }

  private async makePublic(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    /* istanbul ignore next: This state should be unreachable and hence can't be tested */
    if (!this.state.article || !this.isReadyForPublication()) {
      // TODO: The user should not have been able to submit the article
      //       at this point, so log this if it does happen.
      return;
    }

    const periodicalId = (this.state.article.isPartOf && this.state.article.isPartOf.identifier)
      ? this.state.article.isPartOf.identifier
      : undefined;

    this.setState({
      error: undefined,
      isBeingMadePublic: true,
    });

    ReactGA.event({
      action: 'Submit',
      category: 'Article Management',
    });

    try {
      const response = (periodicalId)
        ? await submitScholarlyArticle(this.props.match.params.articleId, periodicalId)
        : await publishScholarlyArticle(this.props.match.params.articleId);

      this.setState((prevState) => ({
        article: {
          ...prevState.article,
          datePublished: response.result.datePublished,
        },
        isBeingMadePublic: false,
      }));
      this.props.onUpdate((
        <span>
          Published!&nbsp;
          <Link
            to={`/article/${this.props.match.params.articleId}`}
            title="View your article's public page"
          >
            Give it a look.
          </Link>
        </span>
      ));
    } catch (e) {
      this.setState({
        error: <span>There was a problem submitting your article, please try again.</span>,
        isBeingMadePublic: false,
      });
    }
  }
}

// The next function is trivial (and forced to be correct through the types),
// but testing it requires inspecting the props of the connected component and mocking the store.
// Figuring that out is not worth it, so we just don't test this:
/* istanbul ignore next */
function mapStateToProps(state: AppState): ArticleManagementPageStateProps {
  return {
    orcid: state.account.orcid,
  };
}
/* istanbul ignore next */
function mapDispatchToArticleManagementPageProps(
  dispatch: ThunkDispatch<AlertState, {}, CompleteSomethingAction | IgnoreCompleteSomethingAction>,
) {
  return {
    onUpdate: (successMessage: JSX.Element) => dispatch(completeSomething(successMessage)),
  };
}

export const ArticleManagementPage =
  connect<ArticleManagementPageStateProps, ArticleManagementPageDispatchProps, ArticleManagementPageOwnProps, AppState>(
    mapStateToProps,
    mapDispatchToArticleManagementPageProps,
  )(
    BareArticleManagementPage,
  );

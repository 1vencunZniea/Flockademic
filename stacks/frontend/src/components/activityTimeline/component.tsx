import * as React from 'react';
import { Link } from 'react-router-dom';

import { Person } from '../../../../../lib/interfaces/Person';
import { PublishedScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { convertToOrcid } from '../../../../../lib/utils/orcid';

interface Props {
  articles: PublishedScholarlyArticle[];
}

export const ActivityTimeline = (props: Props) => (
  props.articles.length === 0
    ? null
    : renderActivity(props.articles)
);

function renderActivity(articles: PublishedScholarlyArticle[]) {
    const list = articles.map((article) => {
      return (
        <div
          className="media"
          itemScope={true}
          itemType="https://schema.org/ScholarlyArticle"
          key={`article_${article.identifier}`}
        >
          <p className="media-content">
            {renderAuthorLink(article.author[0])}
            &nbsp;published&nbsp;
            <Link
              itemProp="url"
              to={`/article/${article.identifier}`}
              title={article.description || 'Access this article for free'}
            >
              {article.name}
            </Link>.
          </p>
        </div>
      );
    });

    return <>{list}</>;
}

function renderAuthorLink(author: Person) {
  if (typeof author.sameAs === 'string') {
    const authorId = convertToOrcid(author.sameAs);

    return (
      <Link
        to={`/profile/${authorId}`}
      >
        {author.name}
      </Link>
    );
  }

  return (
    <strong>{author.name}</strong>
  );
}

jest.mock('../../src/services/fetchUserDashboard', () => ({
  fetchUserDashboard: jest.fn().mockReturnValue(Promise.resolve(
    [
      [
        { identifier: 'arbitrary_periodical_id' },
      ],
      [
        { identifier: 'arbitrary_article_id' },
      ],
    ]
  )),
}));

import { fetchDashboard } from '../../src/resources/fetchDashboard';

const mockContext = {
  database: {} as any,
  session: { identifier: 'Arbitrary ID' },

  body: undefined,
  headers: {},
  method: 'GET' as 'GET',
  params: [ '/dashboard/' ],
  path: '/dashboard/',
  query: null,
};

it('should error when the user\'s journals and articles could not be fetched', () => {
  const mockedService = require.requireMock('../../src/services/fetchUserDashboard');
  mockedService.fetchUserDashboard.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const promise = fetchDashboard(mockContext);

  return expect(promise).rejects.toEqual(new Error('Could not fetch dashboard info.'));
});

it('should call the command handler', (done) => {
  const mockedCommandHandler = require.requireMock('../../src/services/fetchUserDashboard');

  const promise = fetchDashboard({
    ...mockContext,
    params: [ '/dashboard/' ],
    path: '/dashboard/',
    session: { identifier: 'Some creator' },
  });

  setImmediate(() => {
    expect(mockedCommandHandler.fetchUserDashboard.mock.calls.length).toBe(1);
    expect(mockedCommandHandler.fetchUserDashboard.mock.calls[0][1]).toEqual({ identifier: 'Some creator' });
    done();
  });
});

it('should error if the user\'s session could not be found', () => {
  const mockedCommandHandler = require.requireMock('../../src/services/periodical');

  const promise = fetchDashboard({
    ...mockContext,
    params: [ '/dashboard/' ],
    path: '/dashboard/',
    session: new Error('No session found'),
  });

  return expect(promise).rejects.toEqual(new Error('You do not appear to be logged in.'));
});

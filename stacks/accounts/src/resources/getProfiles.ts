import {
  GetProfileResponse,
} from '../../../../lib/interfaces/endpoints/accounts';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { getProfilesForAccounts } from '../dao';

export async function getProfiles(
  context: Request<{}> & DbContext,
): Promise<GetProfileResponse> {
  if (!context.params || context.params.length < 2) {
    throw(new Error('Cannot fetch profile data without account IDs.'));
  }

  const accountIds = context.params[1].split('+');

  try {
    const profiles = await getProfilesForAccounts(context.database, accountIds);

    const normalisedProfiles = profiles.map((profile) => ({
      identifier: profile.identifier,
      name: profile.name,
      sameAs: `https://orcid.org/${profile.orcid}`,
    }));

    return { profiles: normalisedProfiles };
  } catch (e) {
    throw new Error('Could not fetch profile data.');
  }
}
